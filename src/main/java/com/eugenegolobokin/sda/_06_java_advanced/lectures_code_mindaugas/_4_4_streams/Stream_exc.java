package com.eugenegolobokin.sda._06_java_advanced.lectures_code_mindaugas._4_4_streams;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;

public class Stream_exc {

    public static void main(String[] args) {
        // Let's assume we have a list of people.
        List<Person> people = Arrays.asList(
                new Person("Thomas", 55),
                new Person("Jonas", 22),
                new Person("Petras", 33),
                new Person("Thomas", 22)
        );

        // We would like to get the average age of people named "Thomas" w/ streams.
        // - 1. Create stream
        // - 2. Filter Thomas
        // - 3. Get their avg. age (will need to convert to int to get a collection of ints)

        people.stream()
                .filter(person -> person.name.equals("Thomas"))
                .mapToInt(person -> person.age)
                .average()
                .ifPresent(System.out::println);

    }
}

class Person {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

}
