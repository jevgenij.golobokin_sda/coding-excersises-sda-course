package com.eugenegolobokin.sda._06_java_advanced.lectures_code_mindaugas._2_1_interfaces.object_bubblesort;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        ArrayList<Person> people = new ArrayList<>() {
            {
                add(new Person("John", "Snow", 32));
                add(new Person("Jack", "Black", 65));
                add(new Person("Nick", "Junior", 3));
            }
        };

        System.out.println(people);

        bubbleSort(people);
        System.out.println(people);

    }

    public static void bubbleSort(List<Person> personList) {
        for (int i = 0; i < personList.size() - 1; i++) {
            for (int j = 0; j < personList.size() - i - 1; j++) {

                if (personList.get(j).getAge() > personList.get(j + 1).getAge()) {
                    Person temp = personList.get(j);
                    personList.set(j, personList.get(j + 1));
                    personList.set(j + 1, temp);
                }
            }
        }
    }
}
