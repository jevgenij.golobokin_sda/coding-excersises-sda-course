package com.eugenegolobokin.sda._06_java_advanced.lectures_code_mindaugas._2_3_generics;

public class SortedPairExample {
    public static void main(String[] args) {

        // Human mindaugas = new Human("Mindaugas", "B.", 11);
        // Human jonas = new Human("Jonas", "B.", 20);
        // SortedPair<Human> pairOfPeople = new SortedPair<>(mindaugas, jonas);
        // System.out.println(pairOfPeople.getFirst());
        // System.out.println(pairOfPeople.getSecond());

        // Before implementing Comparable
        // Human{firstName='Mindaugas', lastName='B.', age=11}
        // Human{firstName='Jonas', lastName='B.', age=20}

        // After implementing Comparable
        // Human{firstName='Jonas', lastName='B.', age=20}
        // Human{firstName='Mindaugas', lastName='B.', age=11}


        Human mindaugas2 = new Human("Mindaugas", "B.", 11);
        Human jonas2 = new Human("Jonas", "B.", 11);
        SortedPair<Human> pairOfPeople2 = new SortedPair<>(mindaugas2, jonas2);
        System.out.println(pairOfPeople2.getFirst());
        System.out.println(pairOfPeople2.getSecond());
    }
}

class SortedPair<T extends Comparable<T>>{
    private T first;
    private T second;

    public SortedPair(T first, T second) {
        if(first.compareTo(second) < 0){
            this.first = first;
            this.second = second;
        } else {
            this.first = second;
            this.second = first;
        }
    }

    public T getFirst(){
        return first;
    }

    public T getSecond(){
        return second;
    }
}

class Human implements Comparable<Human> {
    private String firstName;
    private String lastName;
    private int age;

    public Human(String firstName, String lastName, int age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public int getAge(){
        return age;
    }

    @Override
    public String toString() {
        return "Human{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Human human) {
        // if (this.age == human.age){
        //     return this.firstName.compareTo(human.firstName);
        // }
        // return Integer.compare(this.age, human.age);

        return this.age == human.age
                ? this.firstName.compareTo(human.firstName)
                : Integer.compare(this.age, human.age);
    }
}
