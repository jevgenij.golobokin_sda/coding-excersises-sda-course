package com.eugenegolobokin.sda._06_java_advanced.lectures_code_mindaugas._3_1_exceptions;

public class Main {
    public static void main(String[] args) {

        // 1. Handling in-built exceptions
        try {
            int i = 5 / 0;
            System.out.println("0: Ar pasieksime šią eilutę?");
        } catch (ArithmeticException e){
            System.out.println("Division by 0");
            e.printStackTrace();
        } catch (Exception e){
            System.out.println("General failure, task failed successfully!");
        } finally {
            System.out.println("1: Ar pasieksime šią eilutę?");
        }

        System.out.println("2: Ar pasieksime šią eilutę?");

        methodThatThrowsAnException();

        // 2. Custom exceptions
        Car car = new Car();
        for (int i = 0; i < 5; i++) {
            try {
                car.increaseSpeed();
            } catch (CarCrashedException e) {
                // e.printStackTrace();
                System.err.println("Gražiau");
            }
        }

        System.out.println("Paskutinė eilutė");
    }

    public static void methodThatThrowsAnException() throws NullPointerException {
        System.out.println("X()");
    }
}


// checked exception
class CarCrashedException extends Exception {
    public CarCrashedException(Car car) {
        // calling Exception(String message) constructor
        super("Car " + car + " has crashed!");
    }
}

class Car {
    private int speed;
    public void increaseSpeed() throws CarCrashedException {
        speed += 70;
        if(speed > 200) {
            throw new CarCrashedException(this);
            // niekas nesivykdo
        }

        // šitoj vietoj?
    }
}
