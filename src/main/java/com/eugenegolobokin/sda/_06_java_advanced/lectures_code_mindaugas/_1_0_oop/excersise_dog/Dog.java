package com.eugenegolobokin.sda._06_java_advanced.lectures_code_mindaugas._1_0_oop.excersise_dog;

public class Dog {
    private String name;
    private int age;
    private String gender;
    private String race;
    private int weight;

    public Dog(String name, int age, String gender, String race, int weight) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.race = race;
        this.weight = weight;
    }

    public Dog(String gender, String race) {
        this("Hop", 3, gender, race, 12);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0) {
            System.out.println("Invalid age");
            this.age = 0;
        } else {
            this.age = age;
        }
    }

    public int getWeight() {
        if (weight < 0) {
            System.out.println("Invalid weight");
            return 0;
        } else {
            return weight;
        }

    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
