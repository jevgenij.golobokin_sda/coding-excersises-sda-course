package com.eugenegolobokin.sda._06_java_advanced.lectures_code_mindaugas._1_0_oop.excersise_dog;

public class DogExe {
    public static void main(String[] args) {

        Dog dog1 = new Dog("Bob", 5, "Male", "Dalmatian", 14);

        Dog dog2 = new Dog("female", "Hound");

        System.out.println(dog1.getAge());
        System.out.println(dog1.getWeight());
        System.out.println(dog2.getAge());
        System.out.println(dog2.getWeight());

    }
}
