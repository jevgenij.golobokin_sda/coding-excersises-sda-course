package cf.mindaugas.sdademoklp._3_3_concurrency;

public class Main {
    public static void main(String[] args) throws InterruptedException{
        // //1. Simple thread
        // StopWatchThread stopWatchThread = new StopWatchThread("Mindaugas");
        // stopWatchThread.start();
        //
        // //0.  Thread sleep
        // System.out.println("Main thread starts");
        // Thread.sleep(3000);
        // System.out.println("Main thread is still running");
        // Thread.sleep(3000);
        // System.out.println("Main thread ends");

        //2. Multiple custom threads
        // StopWatchThread stopWatchThread1 = new StopWatchThread("SW1");
        // StopWatchThread stopWatchThread2 = new StopWatchThread("SW2");
        // stopWatchThread1.start();
        // stopWatchThread2.start();
        // System.out.println("Main thread starts");
        // Thread.sleep(5000);
        // System.out.println("Main thread is still running");
        // Thread.sleep(5000);
        // System.out.println("Main thread ends");

        Thread stopWatchThread = new Thread(new StopWatchThreadWRunnable());
        stopWatchThread.start();
    }
}

class StopWatchThread extends Thread {
    private String prefix;

    StopWatchThread(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Stop watch with prefix : " + this.prefix + " : " + i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


class StopWatchThreadWRunnable implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Stop watch: " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
