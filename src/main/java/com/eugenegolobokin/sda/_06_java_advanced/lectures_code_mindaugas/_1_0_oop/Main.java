package com.eugenegolobokin.sda._06_java_advanced.lectures_code_mindaugas._1_0_oop;

public class Main {

    int someField = 5;

    public static void main(String[] args) {
        System.out.println("Hello World!");

        Person p1 = new Person();

        p1.setAge(150);
        System.out.println(p1.getAge());

        // creating new instance of Main
        // Main m = new Main();
        System.out.println(new Main().someField);
    }
}

class Person {
    private int age;
    private int heightCm;
    private String name;

    public Person(String name){
        // this.age = 0;
        // this.heightCm = 0;
        this(0, 0);
        this.name = name;
    }

    public Person(int age, int heightCm) {
        this.age = age;
        this.heightCm = heightCm;
    }

    public Person(int age, int heightCm, String name) {
        this.age = age;
        this.heightCm = heightCm;
        this.name = name;
    }

    public Person() {
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age >= 150){
            System.out.println("Warning: this age is abnormal for human, keeping default age of 0!");
            this.age = 0;
        } else {
            this.age = age;
        }
    }

    public int getHeightCm() {
        return heightCm;
    }

    public void setHeightCm(int heightCm) {
        this.heightCm = heightCm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

