package com.eugenegolobokin.sda._06_java_advanced.tasks_from_sda.streams;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class StreamsTasks {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>(
                List.of("John",
                        "Sarah",
                        "Mark",
                        "Tyla",
                        "Ellisha",
                        "Eamonn")
        );

        List<Integer> numbers = new ArrayList<>(
                List.of(1, 4, 2346, 123, 76, 11, 0, 0, 62, 23, 50)
        );


        System.out.println(names.toString());
        System.out.println(numbers.toString());

        System.out.println("===== 1. sort list of names");
        sortListOfNames(names);

        System.out.println("===== 2. print names which start with E letter");
        printNamesThatStartWithE(names);

        System.out.println("===== 3. print values greater than 30 and lower than 200");
        printValuesMoreThan30andLessThan200(numbers);

        System.out.println("===== 4. print names uppercase");
        printNamesUpperCase(names);

        System.out.println("===== 5. remove first and last letter, sort and print");
        removeFirstAndLastLettersSortAndPrint(names);

        System.out.println("===== 6. sort backwards using comparator");
        sortBackwardsUsingComparator(names);

    }

    private static void sortBackwardsUsingComparator(List<String> names) {
        names.stream()
                // .sorted(Comparator.comparing(s -> s.toString()).reversed())
                .sorted(Comparator.reverseOrder())
                .forEach(System.out::println);
    }

    private static void removeFirstAndLastLettersSortAndPrint(List<String> names) {
        names.stream()
                .map(s -> s.substring(1, s.length() - 1))
                .sorted()
                .forEach(System.out::println);
    }

    private static void printNamesUpperCase(List<String> names) {
        names.stream()
                .map(String::toUpperCase)
                .forEach(System.out::println);

    }

    private static void printValuesMoreThan30andLessThan200(List<Integer> numbers) {
        numbers.stream()
                .filter(integer -> integer > 30 && integer < 200)
                .forEach(System.out::println);
    }

    public static void sortListOfNames(List<String> names) {
        names.stream()
                .sorted()
                .forEach(System.out::println);
    }

    public static void printNamesThatStartWithE(List<String> names) {
        names.stream()
                .filter(s -> s.startsWith("E"))
                .forEach(System.out::println);
    }
}
