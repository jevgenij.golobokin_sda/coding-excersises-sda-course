package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day5recap.shop;

public class Product implements Priced {

    private Type type;
    private double price;
    private String name;
    private double discount;

    public Product(Type type, double price, String name, double discount) {
        this.type = type;
        this.price = price;
        this.name = name;
        this.discount = discount;
    }

    public Type getType() {
        return type;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    @Override
    public double getDiscount() {
        return discount;
    }

    @Override
    public String toString() {
        return name + " " + price + " " + type;
    }
}
