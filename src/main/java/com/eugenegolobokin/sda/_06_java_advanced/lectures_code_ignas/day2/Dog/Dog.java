package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day2.Dog;

public class Dog {
    private String name;
    private int age = 0;

    public String noise = "bow wow";
    protected int height = 1;
    int weight = 13;

    // modifiers
    //private - tik sitoje klaseje!
    //public - matosi visur
    //protected - tik package!
    //without modifier

    Dog(String name, int age) {

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name != null) {
            this.name = name;
        }
    }

}
