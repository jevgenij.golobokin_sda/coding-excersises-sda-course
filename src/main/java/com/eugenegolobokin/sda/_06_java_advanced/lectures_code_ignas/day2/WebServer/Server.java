package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day2.WebServer;

public class Server {
    public int version = 1;
    public String address = "192.188.142.12";

    private String name = "Server name";

    Server() {

    }

    public void manageDatabase() {
        version++;
        System.out.println("Database was updated to v." + version);
    }
}
