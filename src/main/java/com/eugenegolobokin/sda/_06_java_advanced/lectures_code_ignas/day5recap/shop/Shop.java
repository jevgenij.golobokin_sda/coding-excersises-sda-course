package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day5recap.shop;

import java.util.Scanner;

public class Shop {
    public static void main(String[] args) {
        ProductMenu productMenu = new ProductMenu();

        productMenu.addProduct(new Product(Type.HARDWARE, 500, "Chainsaw", 30));
        productMenu.addProduct(new Product(Type.CLOTHING, 20, "Jeans", 0));
        productMenu.addProduct(new Product(Type.ELECTRONICS, 300, "Monitor", 10));

        System.out.println(productMenu.getProductList());

        ShoppingCart cart = new ShoppingCart();


        System.out.println("Enter choice: add, print, remove, buy, exit");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        while (!input.equals("exit")) {
            switch (input) {
                case "add":
                    System.out.println("Enter product number");
                    input = scanner.nextLine();
                    if (isInputANumber(input)) {
                        int number = Integer.parseInt(input);
                        if (number > 0 && number <= productMenu.getAvailableProducts().size()) {
                            cart.addProduct(productMenu.getAvailableProducts().get(number - 1));
                        }
                    }
                    break;
                case "print":
                    for (Product product : cart.getProductList()) {
                        System.out.println((cart.getProductList().indexOf(product) + 1) + " " + product.toString());
                    }
                    break;
                case "remove":
                    System.out.println("Enter product number");
                    input = scanner.nextLine();
                    if (isInputANumber(input)) {
                        int number = Integer.parseInt(input);
                        if (number > 0 && number <= cart.getProductList().size()) {
                            cart.getProductList().remove(number - 1);
                        }
                    }
                    break;
                case "buy":
                    cart.applyDiscount();
                    System.out.println("Your total is: " + cart.getTotalPrice());
                    break;
            }
            System.out.println("Enter choice: add, print, remove, buy, exit");
            input = scanner.nextLine();
        }

    }

    public static boolean isInputANumber(String input) {
        try {
            Integer.parseInt(input);
        } catch (Exception exception) {
            System.out.println("Invalid input");
            return false;
        }
        return true;
    }


}

