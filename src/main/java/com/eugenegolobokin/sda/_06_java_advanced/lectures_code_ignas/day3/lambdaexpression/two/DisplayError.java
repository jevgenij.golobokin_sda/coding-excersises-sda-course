package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.lambdaexpression.two;

public interface DisplayError {
    public void show();
}
