package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Composition2;

public class User {
    public static void main(String[] args) {
        Phone phone = new Phone("Android IceCream", "3.16", 2400);

        Battery battery = phone.getBattery();
        System.out.println(battery.getBatteryCapacity());

    }
}
