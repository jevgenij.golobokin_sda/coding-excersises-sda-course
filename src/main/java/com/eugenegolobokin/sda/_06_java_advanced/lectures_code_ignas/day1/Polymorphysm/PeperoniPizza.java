package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Polymorphysm;

public class PeperoniPizza extends Pizza {

    String extra = "Spicy";

    PeperoniPizza(int diameter) {
        super(diameter);
        super.name = "Peperoni";
    }
}
