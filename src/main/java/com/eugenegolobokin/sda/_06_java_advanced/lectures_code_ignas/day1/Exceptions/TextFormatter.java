package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Exceptions;

public class TextFormatter {
    private String text;

    public TextFormatter(String text) {
        this.text = text;
    }

    public String toUpperCase() throws NullPointerException {
        return text.toUpperCase();
    }

    public String removeTrailingSpaces() {
        return text.trim();
    }
}
