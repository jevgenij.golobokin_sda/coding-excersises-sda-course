package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.multithreading;

public class ParallelThread extends Thread {
    @Override
    public void run() {
        for (int i  = 0; i<200; i++){

            try {
                Thread.sleep(100);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }

            System.out.println("message from parralel thread number " + i);
        }
    }


}
