package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.multithreading;

import java.util.Random;

public class Race {

    private boolean finished = false;

    public static void main(String[] args) {

        Race race = new Race();
        int trackLength = 50;
        Random random = new Random();

        // java Thread and Runnable usage
        Thread racer1 = new Thread(new Runnable() {
            @Override
            public void run() {
                int mileage = 0;
                System.out.println("Racer one stars the race");
                while (true) {
                    if (mileage == trackLength) {
                        race.finish("one");
                        break;
                    }
                    mileage++;
                    System.out.println("Racer one mileage: " + mileage);
                    try {
                        Thread.sleep((random.nextInt(10) + 1) * 100);
                    } catch (InterruptedException exception) {
                    }
                }

            }
        });

        Thread racer2 = new Thread(() -> {
            int mileage = 0;
            System.out.println("Racer two stars the race");
            while (true) {
                if (mileage == trackLength) {
                    race.finish("two");
                    break;
                }
                mileage++;
                System.out.println("Racer two mileage: " + mileage);
                try {
                    Thread.sleep((random.nextInt(10) + 1) * 100);
                } catch (InterruptedException exception) {
                }
            }
        });


        racer1.start();
        racer2.start();

    }

    //synchronize threads so not to lose resources! access one by one
    public synchronized void finish(String racerNumber) {
        if (!finished) {
            finished = true;
            System.out.println("Racer " + racerNumber + " finished first");
        }
    }
}
