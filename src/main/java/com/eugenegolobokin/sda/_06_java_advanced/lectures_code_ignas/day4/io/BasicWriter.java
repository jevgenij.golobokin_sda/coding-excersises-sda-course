package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class BasicWriter {
    public static void main(String[] args) {

        File file = new File("src\\day4\\io\\text.txt");

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write("Hello file".getBytes());

            fileOutputStream.write((char)60);

            fileOutputStream.write("reader!".getBytes(), 4, 7);

            fileOutputStream.flush();
            fileOutputStream.close();

        } catch (Exception exception) {
            System.err.println("file input/output error");
        }


        if (file.exists()) {
            System.out.println("file exists");

        } else {
            System.out.println("there is no such file");
        }
    }
}
