package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Collections;

public class StudentGradesJournal {
    public static void main(String[] args) {
        Grades grades = new Grades();
        grades.fillStudentGrades();

        grades.printStudentGradesContents();

        System.out.println("##########");

        grades.printStudentGradesFromSet();
    }
}
