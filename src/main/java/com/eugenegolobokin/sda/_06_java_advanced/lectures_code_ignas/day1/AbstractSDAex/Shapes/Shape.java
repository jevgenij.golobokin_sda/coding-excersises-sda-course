package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.AbstractSDAex.Shapes;

public abstract class Shape {
    double perimeter;
    private int area;
    private String color;

    Shape (String color) {
        this.color = color;
    }



    public abstract void calculatePerimeter();

}
