package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.lambdaexpression.two;

public class Runtime {
    public static void main(String[] args) {

        DisplayError displayError = () -> System.out.println("Error");
        displayError.show();

        DisplayMessage displayMessage = message -> System.out.println(message);
        displayMessage.show("Boom!");

    }
}
