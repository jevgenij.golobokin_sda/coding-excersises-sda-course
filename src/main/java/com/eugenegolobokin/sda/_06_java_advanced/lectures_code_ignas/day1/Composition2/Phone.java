package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Composition2;

public class Phone {
   private OS os;
    private Hardware hardware;
    private Battery battery;

    Phone(String osName, String hardwareVersion, int batteryCapacity) {
        os = new OS(osName);
        hardware = new Hardware(hardwareVersion);
        battery = new Battery(batteryCapacity);
    }

    public OS getOs() {
        return os;
    }

    public Hardware getHardware() {
        return hardware;
    }

    public Battery getBattery() {
        return battery;
    }
}
