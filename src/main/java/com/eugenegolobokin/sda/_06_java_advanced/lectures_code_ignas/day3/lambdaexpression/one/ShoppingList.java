package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.lambdaexpression.one;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ShoppingList {

    private List<Item> items;

    // priskirymas su lambda vieno metodo funkcijos
    public ProductPrinter productPrinter = () -> {
        for (Item item : items) {
            System.out.println(item);
        }
    };

    public SingleProductPrinter singleProductPrinter = (name,price) -> {
        System.out.println(name + " " + price);
    };

    public ShoppingList() {
        items = new ArrayList<>();
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public double calculateDiscount(Item item) {
        // function interface - only one method - aprashymas
        Function<Double, Double> discountFunction = new Function<Double, Double>() {
            @Override
            public Double apply(Double aDouble) {
                return aDouble - (aDouble * 0.2);
            }
        };

        // patikrinimas PRIES funkscija
        discountFunction = discountFunction.compose(aDouble -> {
            if (aDouble < 0) {
                return 0.0;
            } else {
                return aDouble;
            }
        });

        // PO funkcijos
        discountFunction = discountFunction.andThen(num -> {
            if (num > 100) {
                return num - 10;
            } else {
                return num;
            }

        });

        // paleisti su APPLY
        return discountFunction.apply(item.getPrice());
    }


    public void printDiscount() {
        Double discount = 0.0;
        items.forEach(item -> {
            System.out.println(calculateDiscount(item)); ;
        });
    }

    public void printItemNames() {
        items.forEach(System.out::println);
    }

    public void calculateDiscount() {
        items.forEach(this::calculateDiscount);
    }


    /* without lambda

        public double calculateDiscount() {
        // function interface - only one method
        Function<Double, Double> discountFunction = new Function<Double, Double>() {
            @Override
            public Double apply(Double aDouble) {
                return aDouble - (aDouble * 0.2);
            }
        };

                discountFunction = discountFunction.compose(new Function<Double, Double>() {
            @Override
            public Double apply(Double aDouble) {
                if (aDouble < 0) {
                    return 0.0;
                } else {
                    return aDouble;
                }
            }
        });

        Item discountable = items.get(0);

        return discountFunction.apply(discountable.getPrice());
    }

     */
}
