package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day2.WebServer;

public class WebPage {
    private String address;
    private int version;
    private String content;

    public WebPage() {

    }

    public WebPage(String address, int version, String content) {
        this.address = address;
        this.version = version;
        this.content = content;
    }

    public void showContent() {
        System.out.println(content);

    }

    public void showInfo() {
        System.out.println(address);
        System.out.println(version);
    }
}
