package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Interfaces;

public class OneLineDatabaseRepository implements CRUD {

    private String oneLineDatabase = "";

    @Override
    public boolean create(String data) {

        System.out.println("Your data to create: " + data);
        boolean isValidForCreation = data != null && !data.equals("");

        if (isValidForCreation) {
            oneLineDatabase = data;
        }

        return isValidForCreation;
    }

    @Override
    public String read() {
        return oneLineDatabase;
    }

    @Override
    public boolean update(String toUpdate, String value) {
        // int isValid = oneLineDatabase.equals(toUpdate) ? 1 : 2;

        if (oneLineDatabase.equals(toUpdate)) {
            oneLineDatabase = value;
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(String data) {
        if (oneLineDatabase.equals(data)) {
            oneLineDatabase = "";
            return true;
        }
        return false;
    }
}
