package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.lambdaexpression.one;

public class Camera implements Item {

    @Override
    public String getName() {
        return "Camera";
    }

    @Override
    public double getPrice() {
        return 200;
    }

    @Override
    public String toString() {
        return getName() + " " + getPrice();
    }
}
