package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Enum;

public class LoginForm {
    private String username;
    private String password;
    public boolean connected;
    private static LoginForm loginForm = null;
    AccessLevel accessLevel;

    public LoginForm(String username, String password) {
        this.username = username;
        this.password = password;
    }


    public boolean login(String username, String password) {
        if (this.username.equals(username) && this.password.equals(password)) {
            if (username.equals(accessLevel.USER.getValue())) {
                accessLevel = AccessLevel.USER;
            } else if (username.equals(accessLevel.MODERATOR.getValue())) {
                accessLevel = AccessLevel.MODERATOR;
            } else if (username.equals(accessLevel.ADMIN.getValue())) {
                accessLevel = AccessLevel.ADMIN;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * prints current access level based on login
     */
    public void checkPrivileges() {

        switch (accessLevel) {
            case USER: {
                System.out.println("Your access level is USER");
            }
            break;
            case MODERATOR: {
                System.out.println("Your access level is MODERATOR");
            }
            break;
            case ADMIN: {
                System.out.println("Your access level is ADMIN");
            }
            break;
        }


    }

    /**
     * checks access level to match ordinal and prints if equal
     * @param ordinal expected access level ordinal
     */
    public void checkPrivileges( int ordinal){
        if (accessLevel.ordinal() == ordinal) {
            System.out.println("Access level correct");
        }
    }

    /**
     * prints out your access level info
     * @param accessLevel  access level to print
     */
    public void checkPrivileges(AccessLevel accessLevel) {
        System.out.println(accessLevel.ordinal());
        System.out.println(accessLevel.name());
    }

    /**
     *
     * @param accessLevel access level
     * @param ordinal expected ordinal
     * @return is your access level equals to ordinal
     */
    public boolean checkPrivileges(AccessLevel accessLevel, int ordinal) {
        return accessLevel.ordinal() == ordinal;
    }

    /**
     * This is too old, please use {@link #checkPrivileges()}
     */
    @Deprecated
    public void privilegePrinter() {

        if (accessLevel == AccessLevel.USER) {
            System.out.println("Your access level is USER");
        } else if (accessLevel == AccessLevel.MODERATOR) {
            System.out.println("Your access level is MODERATOR");
        } else if (accessLevel == AccessLevel.ADMIN) {
            System.out.println("Your access level is ADMIN");
        }
    }
}