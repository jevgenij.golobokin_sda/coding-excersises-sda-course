package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Abstract;

public class SizeDatabase extends Database {
    @Override
    public boolean validate(int data) {
        boolean isValid = data > 0 && data < 200;
        return isValid;
    }
}
