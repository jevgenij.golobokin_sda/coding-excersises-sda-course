package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class FileCopier {

    private String path;

    public FileCopier(String path) {
        this.path = path;
    }

    public void copyTo(String newFilePath) throws Exception {
        FileInputStream fileInputStream = new FileInputStream(path);
        FileOutputStream fileOutputStream = new FileOutputStream(newFilePath);

        int data;
        while ((data = fileInputStream.read()) != -1) {
            System.out.print((char)data + " ");
            fileOutputStream.write(data);
        }

        fileInputStream.close();
        fileOutputStream.flush();
        fileOutputStream.close();
    }

//    public void copyTo(String pathToCpy) throws Exception {
//        FileInputStream inputStream = new FileInputStream(path);
//        FileOutputStream outputStream = new FileOutputStream(pathToCpy);
//        outputStream.write(inputStream.readAllBytes());
//        outputStream.flush();
//        outputStream.close();
//    }

}
