package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Collections;

import java.util.HashSet;
import java.util.Set;

public class UniqueStudentJournal {
    public static void main(String[] args) {
        Set<String> studentList = new HashSet<>();

        studentList.add("John Doe");
        studentList.add("Max Maxwell");
        studentList.add("Max Maxwell");
        studentList.add("John Smith");
        studentList.add("John Smith");

        for (String studentName : studentList) {
            System.out.println(studentName);
        }
    }
}
