package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.io;

import java.io.BufferedInputStream;
import java.io.FileInputStream;

public class Reader {
    public static void main(String[] args) {
        try (FileInputStream fileInputStream = new FileInputStream("src\\day4\\io\\text2.txt")) {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

            int data;
            do {
                data = bufferedInputStream.read();
                System.out.print((char)data + " ");
            } while (data != -1);

        } catch (Exception e){
            System.out.println("IO error");
        }


    }
}
