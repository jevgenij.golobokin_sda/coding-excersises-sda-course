package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.io;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class BufferedWriterExample {

    public static void main(String[] args) {

        try (FileWriter fileWriter = new FileWriter("src\\day4\\io\\text2.txt", true)) {
      /*      //creates new file, overwrites
            fileWriter.write("Hellooooo");
            // adds text to file
            fileWriter.append(" booom");        */

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("Hi, mr file.");
            bufferedWriter.append(" It's me, the buffer");

            bufferedWriter.flush();
            bufferedWriter.close();

        }
        catch (Exception e) {

        }



    }
}
