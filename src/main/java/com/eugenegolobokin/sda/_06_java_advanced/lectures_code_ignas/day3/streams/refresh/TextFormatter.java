package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.streams.refresh;

public interface TextFormatter {
    public String formatText(String text);
}
