package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.generictypes;


public class Cheese implements Item {

    @Override
    public String getName() {
        return "Cheese";
    }

    @Override
    public String getPrice() {
        return "10";
    }
}
