package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.streams;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CompanyCatalog {

    private List<Company> companyList;

    public CompanyCatalog() {
        companyList = new ArrayList<>();
    }

    public void addCompany(Company company) {
        companyList.add(company);
    }

    public List<Company> getItCompanies() {
        List<Company> filteredList = companyList.stream()
                .filter(company -> company.getType().equals("IT"))
                .collect(Collectors.toList());
        return filteredList;

    }

    public void printHighestWorkerCompanies() {
        companyList.stream()
                .map(company -> company.getWorkerCount())
                .filter(workerCount -> workerCount > 20000)
                .forEach(workerCount -> System.out.println(workerCount));
    }

    public Map<String, String> getSmallestCompanies() {
        Map<String, String> companies = companyList.stream()
                .filter(company -> company.getWorkerCount() < 20000)
                .collect(Collectors.toMap(company -> company.getName(), company -> company.getType()));
        return companies;
    }

    public void printSummary() {
       IntSummaryStatistics summary = companyList.stream()
                .mapToInt(company -> company.getWorkerCount())
                .summaryStatistics();

        System.out.println(summary);
    }


}
