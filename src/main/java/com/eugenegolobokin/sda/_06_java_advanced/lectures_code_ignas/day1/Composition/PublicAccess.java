package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Composition;

public class PublicAccess {
    LoginForm loginForm;
    WebPage webPage;

    int userCount = 0;

    PublicAccess(LoginForm inputLoginForm, WebPage inputWebPage) {
        loginForm = inputLoginForm;
        webPage = inputWebPage;

    }

    void validateAccess(String username, String password) {
        if (loginForm.login(username, password)) {
            webPage.displayWebPage();
        }
    }
}
