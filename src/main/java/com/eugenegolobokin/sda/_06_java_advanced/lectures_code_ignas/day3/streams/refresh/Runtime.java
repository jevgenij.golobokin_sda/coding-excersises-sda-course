package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.streams.refresh;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Runtime {
    public static void main(String[] args) {
        // non-lambda anonymous implementation of function interface without return
        MessageFormatter nonLambdaFormatter = new MessageFormatter() {
            @Override
            public void showFormatted(String message) {
                System.out.println(message.toUpperCase());
            }
        };

        // function interface usage with lambda without return
        MessageFormatter formatter = message -> System.out.println(message.toUpperCase());

        nonLambdaFormatter.showFormatted("this is sparta");
        formatter.showFormatted("This is MESsage");


        //streams
        List<Integer> numbers = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            numbers.add(i);
        }

        numbers.stream() // launch stream
                .filter(number -> number % 2 == 0) // filter according needs
                .map(number -> Integer.toString(number)) // mapping into String
                .forEach(number -> System.out.println(number)); // accessing mapped parameters and print out


        // anonymous implementation of function interface with return
        TextFormatter anonymousTextFormatter = new TextFormatter() {
            @Override
            public String formatText(String text) {
                return text.toLowerCase();
            }
        };
        System.out.println(anonymousTextFormatter.formatText("MESsage"));

        // lambda implementation with return
        TextFormatter lambdaTextFormatter = text -> text.toLowerCase();
        System.out.println(lambdaTextFormatter.formatText("FORmatter!"));

        //method reference implementation
        TextFormatter methodReferenceFormatter = String::toLowerCase;
    }
}
