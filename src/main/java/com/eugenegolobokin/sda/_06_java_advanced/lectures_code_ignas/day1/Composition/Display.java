package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Composition;

public class Display {
    public static void main(String[] args) {
        LoginForm loginForm = new LoginForm("user", "password");
        WebPage webPage = new WebPage();

        PublicAccess publicAccess = new PublicAccess(loginForm, webPage);
        publicAccess.validateAccess("user", "password");
    }
}
