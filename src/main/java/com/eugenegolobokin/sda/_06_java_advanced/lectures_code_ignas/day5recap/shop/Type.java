package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day5recap.shop;

public enum Type {
    CLOTHING,
    HARDWARE,
    ELECTRONICS
}
