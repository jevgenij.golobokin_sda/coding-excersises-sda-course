package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day2.Products;

public class Product {
    private String name;
    private Units units;

    public Product(Units units, String name) {
        this.units = units;
        this.name = name;
    }


    @Override
    public String toString() {
        return name + units.name();
    }
}
