package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Collections;

import java.util.ArrayList;
import java.util.List;

public class Students {
    private List<String> studentList;

    public Students() {
        studentList = new ArrayList<>();
    }

    public void addStudent(String studentName) {
        studentList.add(studentName);
    }

    public boolean isStudentInClass(String studentName) {
        return studentList.contains(studentName);
    }

    /**
     * @return -1 for no student in list
     */
    public int getStudentPosition(String studentName) {
        return studentList.indexOf(studentName);
    }

    public String findStudentAtPosition(int position) {
        return studentList.get(position);
    }

    public String removeStudent(int position) {
        return studentList.remove(position);
    }

    public boolean removeStudent(String studentName) {
        return studentList.remove(studentName);
    }

    public int studentCount() {
        return studentList.size();
    }

    public void clearStudentList() {
        studentList.clear();
    }

    public List<String> getStudentList() {
        return studentList;
    }

}
