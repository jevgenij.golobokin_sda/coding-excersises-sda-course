package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.multithreading;

import java.util.concurrent.atomic.AtomicInteger;

public class ResourceManagement {

    public static int counter = 0;
    public static AtomicInteger atomicInteger = new AtomicInteger(0);

    public static void main(String[] args) {
        Thread counterIncrease = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                increaseCounter();

                atomicInteger.addAndGet(1);
            }
            System.out.println("Thread 1 finish " + counter);
            System.out.println("Thread 1 atomic " + atomicInteger.get());
        });

        Thread counterIncrease1 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                increaseCounter();

                atomicInteger.addAndGet(1);
            }
            System.out.println("Thread 2 finish " + counter);
            System.out.println("Thread 2 atomic " + atomicInteger.get());
        });


        counterIncrease.start();
        counterIncrease1.start();


    }

    public static synchronized void increaseCounter() {
        counter++;
    }
}
