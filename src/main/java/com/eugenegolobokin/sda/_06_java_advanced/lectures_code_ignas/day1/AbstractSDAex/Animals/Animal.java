package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.AbstractSDAex.Animals;

public abstract class Animal {
    private int age;
    private boolean isFlying;
    private boolean isSwimming;

    Animal() {

    }

    public Animal(int age, boolean isFlying, boolean isSwimming) {
        this.age = age;
        this.isFlying = isFlying;
        this.isSwimming = isSwimming;

    }

    public abstract void eat();


}
