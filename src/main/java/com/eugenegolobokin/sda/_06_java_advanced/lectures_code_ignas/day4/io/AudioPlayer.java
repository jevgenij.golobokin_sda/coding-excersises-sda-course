package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.io;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;
import java.io.IOException;

public class AudioPlayer {

    private Clip audioClip;
    private long trackPosition;

    public AudioPlayer(String path) throws Exception {

        File file = new File(path);
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
        audioClip = AudioSystem.getClip();
        audioClip.open(audioInputStream);
        trackPosition = 0;
    }

    public void play() {
        audioClip.setMicrosecondPosition(trackPosition);
        audioClip.start();
    }

    public void pause() {
        trackPosition = audioClip.getMicrosecondPosition();
        audioClip.stop();
    }

    public void eject() {
        audioClip.stop();
        audioClip.close();
    }

    public void forward() {
        pause();
        trackPosition += 1000000;
        play();
    }
}
