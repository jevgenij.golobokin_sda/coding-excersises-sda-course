package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Enum;

import java.util.Scanner;

public class AccessChecker {
    public static void main(String[] args) {
        LoginForm loginForm = new LoginForm("user", "admin");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter username:");
        String inputUsername = scanner.nextLine();
        System.out.println("Enter password:");
        String inputPassword = scanner.nextLine();

        boolean loginSuccessful = loginForm.login(inputUsername, inputPassword);
        if (loginSuccessful) {
            loginForm.checkPrivileges();
           // loginForm.privilegePrinter();
        }

        for (AccessLevel level : AccessLevel.values()) {
            loginForm.checkPrivileges(level);
            if (loginForm.checkPrivileges(level, 0)) {
                System.out.println("This is first element.");
            }
        }

        loginForm.checkPrivileges(2);
    }
}
