package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.io;

public class MainProgram {
    public static void main(String[] args) {

        FileCopier fileCopier = new FileCopier("src\\day4\\io\\image.png");

        try {
            fileCopier.copyTo("src\\day4\\io\\image2.png");
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }
}
