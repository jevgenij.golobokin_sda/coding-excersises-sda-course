package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.lambdaexpression.one;

public class WebShop {

    public static void main(String[] args) {
        ShoppingList shoppingList = new ShoppingList();
    Camera camera = new Camera();
       shoppingList.addItem(camera);

        //pridejimas anoniminio objekto is interfejso, iskar overridinant jo metodus
//        shoppingList.addItem(new Item() {
//            @Override
//            public String getName() {
//                return "Anonymous item";
//            }
//
//            @Override
//            public double getPrice() {
//                return 200;
//            }
//        });
//
//        double priceWithDiscount = shoppingList.calculateDiscount(camera);
//
//        System.out.println("Discounted price: " + priceWithDiscount);

        shoppingList.printDiscount();

        shoppingList.productPrinter.print();

        shoppingList.singleProductPrinter.print(camera.getName(), camera.getPrice());
    }
}
