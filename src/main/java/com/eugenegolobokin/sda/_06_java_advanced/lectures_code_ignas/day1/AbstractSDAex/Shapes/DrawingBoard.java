package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.AbstractSDAex.Shapes;

public class DrawingBoard {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle("Red", 3, 5);
        rectangle.calculatePerimeter();

        Circle circle = new Circle("Yellow", 7);
        circle.calculatePerimeter();

    }
}
