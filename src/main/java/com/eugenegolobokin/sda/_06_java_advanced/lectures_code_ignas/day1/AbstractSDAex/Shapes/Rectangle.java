package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.AbstractSDAex.Shapes;

public class Rectangle extends Shape {

    private int sideA;
    private int sideB;


    Rectangle(String color, int sideA, int sideB) {
        super(color);
        this.sideA = sideA;
        this.sideB = sideB;

    }

    @Override
    public void calculatePerimeter() {
        perimeter = 2*sideA + 2*sideB;
        System.out.println("Perimeter is: " + perimeter);
    }
}
