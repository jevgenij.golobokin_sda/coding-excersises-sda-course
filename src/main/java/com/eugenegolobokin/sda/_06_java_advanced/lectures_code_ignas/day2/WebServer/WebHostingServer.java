package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day2.WebServer;

public class WebHostingServer extends Server {

    WebHostingServer() {
        version = 5;
        address = "122.154.89.5";
    }

    public void showWebPage() {
        System.out.println("Web.");
    }
}
