package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Interfaces;

public interface CRUD {
    public boolean create(String data);
    public String read();
    public boolean update(String toUpdate, String value);
    public boolean delete(String data);


}
