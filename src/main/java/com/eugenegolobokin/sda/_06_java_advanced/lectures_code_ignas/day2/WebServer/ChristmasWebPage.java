package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day2.WebServer;

public class ChristmasWebPage extends WebPage {

    public ChristmasWebPage(String address, int version, String content) {
        //constructor from father class
        super(address, version, content);
    }

    @Override
    public void showContent() {
        super.showContent();

        String christmasTree = "*";
        for (int i = 0; i < 7; i++) {
            System.out.println(christmasTree);
            christmasTree += "*";
        }
    }
}
