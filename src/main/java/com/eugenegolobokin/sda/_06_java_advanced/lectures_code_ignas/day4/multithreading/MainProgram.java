package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.multithreading;

public class MainProgram {
    public static void main(String[] args)  {

        // start parallel thread -> use start(), but not run method
        ParallelThread parallelThread = new ParallelThread();
        parallelThread.start();

        for (int i = 0; i < 200;i++){

            try {
                Thread.sleep(100);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }

            System.out.println("Main message number " + i);
        }
    }
}
