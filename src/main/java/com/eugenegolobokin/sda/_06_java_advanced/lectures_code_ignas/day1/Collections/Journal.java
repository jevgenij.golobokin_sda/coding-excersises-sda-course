package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Collections;

public class Journal {

    public static void main(String[] args) {
        Students students = new Students();

        students.addStudent("Jack Black");
        students.addStudent("Joe Doe");
        students.addStudent("Jack Black");
        students.addStudent("Joe Doe");
        students.addStudent("Van Gogh");

        System.out.println(students.findStudentAtPosition(0));

        int position = students.getStudentPosition("Tom Hanks");
        if (position >= 0) {
            System.out.println("Student in class");
        } else {
            System.out.println("Student not present");
        }

        System.out.println("Student list: #########");
        // print out list with for-each list
        for (String studentName : students.getStudentList()) {
            System.out.println(studentName);
        }

        // print out List with Iterator
 /*       Iterator<String> studentIterator = students.getStudentList().iterator();
        while (studentIterator.hasNext()) {
            System.out.println(studentIterator.next());
            studentIterator.remove();
        } */

    }
}
