package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Composition2;

public class OS {
    private String osVersion;

    OS(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getOsVersion() {
        return osVersion;
    }
}
