package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Interfaces;

public interface DefaultMethod {
    public void worker();

    // can avoid static final in interfaces if value is provided!
    public boolean isWorking = true;

    public default void defaultWorker() {

        System.out.println(" this is default ");
    }
}
