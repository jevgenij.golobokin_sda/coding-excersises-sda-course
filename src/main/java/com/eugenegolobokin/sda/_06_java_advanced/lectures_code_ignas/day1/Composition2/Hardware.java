package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Composition2;

public class Hardware {

    private String hardwareVersion;

    Hardware(String hardwareVersion) {
        this.hardwareVersion = hardwareVersion;
    }

    public String getHardwareVersion() {
        return hardwareVersion;
    }
}
