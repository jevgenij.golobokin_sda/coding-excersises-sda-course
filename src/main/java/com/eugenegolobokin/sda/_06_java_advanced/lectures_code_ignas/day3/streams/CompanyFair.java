package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.streams;

import org.w3c.dom.ls.LSOutput;

import java.util.List;
import java.util.stream.Stream;

public class CompanyFair {


    public static void main(String[] args) {
        CompanyCatalog companyCatalog = new CompanyCatalog();
        companyCatalog.addCompany(new Company("Microsoft", "IT", 9999));
        companyCatalog.addCompany(new Company("Google", "IT", 50000));
        companyCatalog.addCompany(new Company("McDonald", "Food", 30000));
        companyCatalog.addCompany(new Company("John Electrics", "Hardware", 15000));

        List<Company> itCompanies = companyCatalog.getItCompanies();

        itCompanies.forEach(company -> System.out.println(company.getName() + " " + company.getType()));

        System.out.println("==============");
        companyCatalog.printHighestWorkerCompanies();

        System.out.println("==============");
        companyCatalog.getSmallestCompanies().forEach( (name,type) -> {
            System.out.println(name + " " + type);
        });

        System.out.println("==============");
        companyCatalog.printSummary();

        System.out.println("==============");
        // for (int i = 0;  ; i=i+1)
        Stream.iterate(0, i->i+1)
                .filter(i->i%2 == 0)
                .limit(4)
                .forEach(System.out::println);
    }
}
