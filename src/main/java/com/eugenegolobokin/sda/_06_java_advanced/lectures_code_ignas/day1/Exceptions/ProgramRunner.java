package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ProgramRunner {
    public static void main(String[] args) {
      //  TextFormatter textFormatter = new TextFormatter(null);
        String outputText = "";

        try (Scanner scanner = new Scanner(System.in)) {
            String inputText;
            inputText = scanner.nextLine();
            if(inputText.equals("")) {
                throw new InputMismatchException();
            }
            outputText = new TextFormatter(inputText).toUpperCase();

        } catch (NullPointerException nullPointerException) {
            outputText = "Invalid input";
        } catch (Exception exception) {
            System.out.println(exception.toString());
            System.exit(0); // can send logs and backtraces to devs?
        }

        System.out.println(outputText);
    }
}
