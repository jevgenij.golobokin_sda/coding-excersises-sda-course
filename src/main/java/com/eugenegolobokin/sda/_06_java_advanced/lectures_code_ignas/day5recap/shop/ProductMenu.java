package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day5recap.shop;

import java.util.ArrayList;
import java.util.List;

public class ProductMenu {

    private List<Product> availableProducts;

    public ProductMenu() {
        availableProducts = new ArrayList<>();
    }

    public List<Product> getAvailableProducts() {
        return availableProducts;
    }

    public void addProduct(Product product) {
        availableProducts.add(product);
    }

    public String getProductList() {
        StringBuilder list = new StringBuilder();
        for (Product product : availableProducts) {
            list.append(product.toString()).append("\n");
        }
        return list.toString();
    }
}
