package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Collections;

import java.util.HashMap;
import java.util.Map;

public class Grades {
    private Map<String, Integer> studentGrades;

    public Grades() {
        studentGrades = new HashMap<>();
    }

    public void fillStudentGrades() {
        studentGrades.put("Jack Black", 10);
        studentGrades.put("Jack Black", 9);
        studentGrades.put("Joe Doe", 4);
        studentGrades.put("Joe Doe", 10);
        studentGrades.put("Van Gogh", 4);

    }

    public void printStudentGradesContents() {
        for (String key : studentGrades.keySet()) {
            System.out.println("Student " + key);
            System.out.println("Grade is: " + studentGrades.get(key));
        }

    }

    // print out HashMap contents version with Entry
    public void printStudentGradesFromSet() {
        for (Map.Entry<String, Integer> entry : studentGrades.entrySet())
         {
            System.out.println("Student " + entry.getKey());
            System.out.println("Grade is: " + entry.getValue());
        }
    }
}
