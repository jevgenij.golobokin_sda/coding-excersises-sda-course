package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Polymorphysm;

public class TonysPizza {

    //factory
    public Pizza createPineapplePizza(int diameter) {
        return new PineapplePizza(diameter);
    }

    public Pizza createPepperoniPizza(int diameter) {
        return new PeperoniPizza(diameter);
    }

    public Pizza createVegetarianPizza(int diameter) {
        return new VegetarianPizza(diameter);
    }
}
