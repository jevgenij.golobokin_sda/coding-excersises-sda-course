package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.AbstractSDAex.Shapes;

public class Circle extends Shape {

    private int radius;

    Circle(String color, int radius) {
        super(color);
        this.radius = radius;
    }

    @Override
    public void calculatePerimeter() {
        perimeter = radius * 2 * 3.14;
        System.out.println("circumference is: " + perimeter);

    }


}
