package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Abstract;

public abstract class Database {
    private int version;

    Database() {
        version = 0;
    }

    public void insert(int data) {
        if (validate(data)) {
            System.out.println("data: " + data + " inserted");
        }

    }

    public abstract boolean validate(int data);


}
