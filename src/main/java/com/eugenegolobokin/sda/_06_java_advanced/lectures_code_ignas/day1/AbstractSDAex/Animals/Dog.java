package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.AbstractSDAex.Animals;

public class Dog extends Animal {

    int height;
    int weight;

    Dog(int age, boolean isFlying, boolean isSwimming, int height, int weight) {
        super(age, isFlying, isSwimming);
        this.height = height;
        this.weight = weight;
    }

    public void bark() {
        System.out.println("Bow wow");
    }

    @Override
    public void eat() {
        System.out.println("day1.day2.Dog eats meatballs");
    }


}
