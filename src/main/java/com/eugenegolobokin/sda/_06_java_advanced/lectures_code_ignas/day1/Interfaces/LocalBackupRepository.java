package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Interfaces;

public class LocalBackupRepository implements CRUD {



    @Override
    public boolean create(String data) {
        System.out.println("saved locally: " + data);
        return true;
    }

    @Override
    public String read() {
        System.out.println("read from local repository: ");
        return "";
    }

    @Override
    public boolean update(String toUpdate, String value) {
        System.out.println("updated locally: " + toUpdate + " to " + value);
        return true;
    }

    @Override
    public boolean delete(String data) {
        System.out.println("deleted from local repository: " + data);
        return true;
    }
}
