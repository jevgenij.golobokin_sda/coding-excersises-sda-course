package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day2.WebServer;

import java.util.Scanner;

public class WebServer {
    public static void main(String[] args) {
        //LoginForm loginForm = new LoginForm("user", "admin");

        LoginForm loginForm = LoginForm.getInstance();

        Scanner scanner = new Scanner(System.in);

        String userName = scanner.nextLine();



        boolean loginSuccessful = loginForm.login("user", "admin");

        while (!loginSuccessful) {

            if (loginSuccessful) {

                System.out.println("Login successful");
                loginForm.connected = true;

                System.out.println("Would you like to change username?");
                String changeUsernameInput = scanner.nextLine();

                if (changeUsernameInput.equals("yes")) {
                    System.out.println("Enter new username: ");
                    String newUserName = scanner.nextLine();
                    loginForm.setUsername(newUserName);
                }

                System.out.println("Would you like to change password?");
                String changePasswordInput = scanner.nextLine();

                if (changePasswordInput.equals("yes")) {
                    System.out.println("Enter new password: ");
                    String newPassword = scanner.nextLine();
                    loginForm.setPassword(newPassword);
                }

            } else {
                System.out.println("Login failed");
                System.out.println("Are you sure you are " + loginForm.getUsername());
                loginForm.connected = false;
            }

        }


        scanner.close();
    }
}

