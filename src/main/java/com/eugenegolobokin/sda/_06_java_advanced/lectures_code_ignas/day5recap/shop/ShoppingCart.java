package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day5recap.shop;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private List<Product> productList;

    private PriceCalculator<Product> priceCalculator;

    public ShoppingCart() {
        productList = new ArrayList<>();

        priceCalculator = new PriceCalculator<>(productList);
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void addProduct(Product product) {
        productList.add(product);

    }

    public void applyDiscount() {
        priceCalculator.applyDiscount();
    }

    public double getTotalPrice() {
        priceCalculator.calculate();

        return priceCalculator.getTotal();
    }
}
