package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3;

public class fibo {
    public static void main(String[] args) {
        fib(100);
    }

    public static int fib(int n) {
        if (n <= 1)
            return n;
        return fib(n - 1) + fib(n - 2); }
}
