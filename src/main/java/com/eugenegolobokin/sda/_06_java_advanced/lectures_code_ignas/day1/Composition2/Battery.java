package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Composition2;

public class Battery {
    private int batteryCapacity;

    public Battery(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }
}
