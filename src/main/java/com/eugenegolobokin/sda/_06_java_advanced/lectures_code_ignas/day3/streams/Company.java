package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.streams;

public class Company {
    private String name;
    private String type;
    private int workerCount;

    public Company(String name, String type, int workerCount) {
        this.name = name;
        this.type = type;
        this.workerCount = workerCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getWorkerCount() {
        return workerCount;
    }

    public void setWorkerCount(int workerCount) {
        this.workerCount = workerCount;
    }
}
