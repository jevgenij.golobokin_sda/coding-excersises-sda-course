package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Interfaces;

public class CloudRepository implements CRUD {

    @Override
    public boolean create(String data) {
        return false;
    }

    @Override
    public String read() {
        return null;
    }

    @Override
    public boolean update(String toUpdate, String value) {
        return false;
    }

    @Override
    public boolean delete(String data) {
        return false;
    }
}
