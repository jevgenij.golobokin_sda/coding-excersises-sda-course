package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day2.WebServer;

public class AdvancedWebHostingServer extends WebHostingServer {

    AdvancedWebHostingServer() {
        version = 90;
    }

    void doAdvancedHosting() {
        System.out.println("Advanced hosting");
    }

    @Override
    public void showWebPage() {
        System.out.println("Advanced web");
    }

    @Override
    public String toString() {
        return "my web hosting server";
    }
}
