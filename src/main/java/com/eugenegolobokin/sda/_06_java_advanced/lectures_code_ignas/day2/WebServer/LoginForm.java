package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day2.WebServer;

public class LoginForm {
    private String username;
    private String password;
    public boolean connected;
    private static LoginForm loginForm = null;

    LoginForm(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public static LoginForm getInstance() {
        if (loginForm == null) {
            loginForm = new LoginForm("user", "admin");
        }
            return loginForm;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean login(String username, String password) {
        if (this.username.equals(username) && this.password.equals(password)) {
            return true;
        } else {
            return false;
        }
    }
}
