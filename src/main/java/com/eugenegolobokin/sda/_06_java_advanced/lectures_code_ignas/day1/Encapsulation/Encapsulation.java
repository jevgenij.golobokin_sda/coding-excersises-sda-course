package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Encapsulation;

public class Encapsulation {
    private String firstName;
    private String lastName;
    public Encapsulation() {
    }
    public Encapsulation(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public static void main(String[] args) {
        Encapsulation obj = new Encapsulation("John", "Doe");
        System.out.println("========");
        System.out.println(obj.firstName);
        System.out.println(obj.getFirstName());
        System.out.println("========2222");
        obj.firstName = "Pavel";
        System.out.println(obj.firstName);
        System.out.println(obj.getFirstName());
    }//psvm
}//class ends...