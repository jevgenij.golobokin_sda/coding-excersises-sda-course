package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Polymorphysm;

public class JohnPizza {

    public static Pizza bakePizza(int diameter, String ingredient) {

        if (ingredient.equals("peperoni")) {
            return new PeperoniPizza(diameter);
        } else if (ingredient.equals("pineapple")) {
            return new PineapplePizza(diameter);
        } else {
            return new VegetarianPizza(diameter);
        }

    }
}
