package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Enum;

public enum AccessLevel {
    USER("user"),
    MODERATOR ("moderator"),
    ADMIN ("admin");

    private String value;

    AccessLevel(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


}
