package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.generictypes;

import java.util.ArrayList;
import java.util.List;

// generic for adding items
// when extends - we can implement methods not only for writing
// extends - konkretizuoja kokio tipo gali ateiti (type kuria naudoja Item interfejsa)
public class ShoppingCart<T extends Item> {

    List<T> cart;

    public ShoppingCart() {
        cart = new ArrayList<>();
    }

    public void addItem(T item) {
        cart.add(item);
        System.out.println("Item added");
    }

    public List<T> getItems() {
        return cart;
    }

    public void printItems() {
        for (Item item : cart) {
            System.out.println(item.getName() + " : " + item.getPrice());
        }
    }

    public static void main(String[] args) {
        ShoppingCart<Item> shoppingCart = new ShoppingCart<>();
        Cheese cheese = new Cheese();
        shoppingCart.addItem(cheese);

        shoppingCart.printItems();
    }

}
