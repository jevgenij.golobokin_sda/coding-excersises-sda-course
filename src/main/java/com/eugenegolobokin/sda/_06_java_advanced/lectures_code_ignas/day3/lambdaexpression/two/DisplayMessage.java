package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.lambdaexpression.two;

public interface DisplayMessage {
    public void show(String message);
}
