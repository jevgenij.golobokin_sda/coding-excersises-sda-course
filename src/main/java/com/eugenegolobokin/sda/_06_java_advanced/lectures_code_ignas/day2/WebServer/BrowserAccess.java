package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day2.WebServer;

public class BrowserAccess {
    public static void main(String[] args) {
        Server server = new Server();
        server.manageDatabase();

        WebHostingServer webHostingServer = new WebHostingServer();
        webHostingServer.manageDatabase();
        webHostingServer.showWebPage();

        AdvancedWebHostingServer advancedWebHostingServer = new AdvancedWebHostingServer();
        advancedWebHostingServer.manageDatabase();
        advancedWebHostingServer.showWebPage();

        ChristmasWebPage christmasWebPage = new ChristmasWebPage("3.14.154.22", 3, "Hello, web");
        christmasWebPage.showContent();
    }
}
