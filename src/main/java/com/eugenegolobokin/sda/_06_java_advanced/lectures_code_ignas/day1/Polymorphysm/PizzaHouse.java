package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Polymorphysm;

public class PizzaHouse {
    public static void main(String[] args) {
        TonysPizza tonysPizza = new TonysPizza();

        Pizza pizza = tonysPizza.createPineapplePizza(32);

        System.out.println("got tat super " + pizza.name + "pizza !!");


        Pizza johnPizza = JohnPizza.bakePizza(32, "peperoni");

        System.out.println("Gimme dat peperoni, John!");
        System.out.println("Here you go - your " + johnPizza.name + " pizza");

        // cast to parent class - to access variable from child class - NOT RECOMMENDED
        System.out.println("extras: " + ((PeperoniPizza) johnPizza).extra);
    }
}
