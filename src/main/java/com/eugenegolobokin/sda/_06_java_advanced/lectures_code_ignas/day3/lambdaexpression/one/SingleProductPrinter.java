package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.lambdaexpression.one;

public interface SingleProductPrinter {
    public void print (String name, double price);
}
