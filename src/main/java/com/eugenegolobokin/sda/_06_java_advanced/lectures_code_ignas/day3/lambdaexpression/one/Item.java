package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.lambdaexpression.one;

public interface Item {
    public String getName();
    public double getPrice();
}
