package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Encapsulation;

class Stuff extends Encapsulation {
    public static void main(String[] args) {
        Encapsulation obj = new Encapsulation("yohan","wadia");
        System.out.println("========");
        //System.out.println(obj.firstName);
        //obj.firstName = "Pavel";
        obj.setFirstName("pavel");
        System.out.println(obj.getFirstName());
    }
}