package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Polymorphysm;

public class VegetarianPizza extends Pizza {

    VegetarianPizza(int diameter) {
        super(diameter);
        super.name = "Vegetarian";
    }
}
