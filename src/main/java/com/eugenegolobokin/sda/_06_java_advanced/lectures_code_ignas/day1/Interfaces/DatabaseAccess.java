package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Interfaces;

import java.util.Scanner;

public class DatabaseAccess {
    public static void main(String[] args) {
        CRUD repository = new LocalBackupRepository();
        Scanner scanner = new Scanner(System.in);

        String input = "";

        while (!input.equals("quit")) {
            System.out.println("insert command: ");
            input = scanner.nextLine();

            switch (input) {
                case "create":
                    System.out.println("enter value to create database entry: ");
                    input = scanner.nextLine();

                    if (repository.create(input)) {
                        System.out.println("data created");
                    } else {
                        System.out.println("wrong input");
                    }
                    break;

                case "read":
                    String value = repository.read();
                    System.out.println(value);
                    break;

                case "update":
                    System.out.println("please enter which value to update:");
                    String oldEntry = scanner.nextLine();

                    System.out.println("please enter new value: ");
                    String newValue = scanner.nextLine();

                    if (repository.update(oldEntry, newValue)) {
                        System.out.println("update successful");
                    } else {
                        System.out.println("wrong input");
                    }
                    break;

                case "delete":
                    System.out.println("enter value to delete:");
                    input = scanner.nextLine();

                    if (repository.delete(input)) {
                        System.out.println("delete successful");
                    } else {
                        System.out.println("deletion failed - no value found");
                    }
                    break;
            }

        }


    }
}
