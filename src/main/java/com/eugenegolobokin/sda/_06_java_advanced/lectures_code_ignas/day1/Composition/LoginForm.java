package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day1.Composition;

public class LoginForm {
    String username;
    String password;

    LoginForm(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public boolean login(String username, String password) {
        if (this.username.equals(username) && this.password.equals(password)) {
            return true;
        } else {
            return false;
        }
    }
}
