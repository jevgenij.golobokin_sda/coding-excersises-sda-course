package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day3.lambdaexpression.one;

public class Phone implements Item {
    @Override
    public String getName() {
        return "Phone";
    }

    @Override
    public double getPrice() {
        return 400;
    }
}
