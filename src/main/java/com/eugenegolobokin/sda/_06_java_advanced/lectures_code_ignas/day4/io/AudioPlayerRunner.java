package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day4.io;

import java.util.Scanner;

public class AudioPlayerRunner {
    public static void main(String[] args) {
        try {
            AudioPlayer audioPlayer = new AudioPlayer("src\\day4\\io\\audio.wav");

            System.out.println("Enter: play  pause  stop");

            Scanner scanner = new Scanner(System.in);

            String userInput = scanner.nextLine();

            while (!userInput.equals("stop")) {

                switch (userInput) {
                    case "play": {
                        audioPlayer.play();
                        break;
                    }
                    case "pause": {
                        audioPlayer.pause();
                        break;
                    }
                    case "stop": {
                        audioPlayer.eject();
                        break;
                    }
                    case "ff": {
                        audioPlayer.forward();
                        break;
                    }
                }
                userInput = scanner.nextLine();
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }


    }
}
