package com.eugenegolobokin.sda._06_java_advanced.lectures_code_ignas.day5recap.shop;

import java.util.List;
import java.util.stream.Collectors;

public class PriceCalculator<T extends Priced> {
    private List<T> productList;
    private boolean isDiscountApplied;
    private double total;

    public PriceCalculator(List<T> productList) {
        this.productList = productList;
        isDiscountApplied = false;
        total = 0;
    }

    public void applyDiscount() {
        isDiscountApplied = true;
    }

    public void calculate() {
        List<T> nonDiscounted = productList.stream()
                .filter(priced -> priced.getDiscount() == 0)
                .collect(Collectors.toList());

        List<T> discounted = productList.stream()
                .filter(priced -> priced.getDiscount() > 0)
                .collect(Collectors.toList());

        for (T priced : nonDiscounted) {
            total += priced.getPrice();
        }

        for (T priced : discounted) {
            total += calculateDiscount(priced);
        }

        if (isDiscountApplied) {
            for (T priced : productList) {
                total += calculateDiscount(priced);
            }
        }
    }

    private double calculateDiscount(T priced) {
        return priced.getPrice() - (priced.getDiscount() / 100 * priced.getPrice());
    }

    public double getTotal() {
        return total;
    }
}
