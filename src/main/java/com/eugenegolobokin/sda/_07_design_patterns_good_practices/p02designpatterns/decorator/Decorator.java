package com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.decorator;


import com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.decorator.example.BasicPizza;
import com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.decorator.example.HamPizza;
import com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.decorator.example.MushroomsPizza;
import com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.decorator.example.Pizza;

public class Decorator {

    public static void main(String[] args) {
        Pizza pizza = new BasicPizza();
        pizza.printIngredients();
        Pizza hamPizza = new HamPizza(pizza);
        pizza.printIngredients();
        Pizza mushroomsPizza = new MushroomsPizza(pizza);
        pizza.printIngredients();

//        Pizza seafoodPizza = new SeafoodPizza(new BasicPizza());
//        pizza.printIngredients();
    }

}