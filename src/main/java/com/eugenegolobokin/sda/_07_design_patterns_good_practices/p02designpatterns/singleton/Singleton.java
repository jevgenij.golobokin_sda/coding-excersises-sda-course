package com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.singleton;


import com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.singleton.example.Pizza;

public class Singleton {

    public static void main(String[] args) {

        // Pizza pizza = new Pizza - NOT POSSIBLE!!!
        Pizza margharita = Pizza.getInstance();
        margharita.name = "Margharita";
        Pizza capriciosa = Pizza.getInstance();
        capriciosa.name = "Capriciosa";
        Pizza fruttidimare = Pizza.getInstance();
        fruttidimare.name = "Frutti di mare";

        System.out.println(margharita);
        System.out.println(capriciosa);
        System.out.println(fruttidimare);

//        Animal dog = Animal.getInstance();
//        Animal cat = Animal.getInstance();
//        cat.type = "Cat";
//        Animal duck = Animal.getInstance();
//        duck.type = "Duck";
//
//        System.out.println(dog);
//        System.out.println(cat);
//        System.out.println(duck);
    }
}