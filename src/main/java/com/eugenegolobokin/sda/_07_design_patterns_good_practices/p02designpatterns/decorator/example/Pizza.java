package com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.decorator.example;

public interface Pizza {
    public void addIngredients(String ingredient);
    public void printIngredients();
}
