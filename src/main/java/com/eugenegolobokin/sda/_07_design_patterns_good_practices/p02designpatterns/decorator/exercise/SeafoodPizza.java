package com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.decorator.exercise;


import com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.decorator.example.Pizza;
import com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.decorator.example.PizzaDecorator;

public class SeafoodPizza extends PizzaDecorator {

    public SeafoodPizza(Pizza pizza) {
        super(pizza);
        super.addIngredients("Seafood");
    }

    @Override
    public void printIngredients() {
        super.printIngredients();
    }
}
