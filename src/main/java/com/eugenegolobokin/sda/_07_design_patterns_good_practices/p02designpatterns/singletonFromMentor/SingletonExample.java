package com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.singletonFromMentor;

public class SingletonExample {
    public static void main(String[] args) {

        // Pizza p = new Pizza(); // not possible!!!
        Pizza margharita = Pizza.getInstance();
        margharita.name = "Margharita";
        Pizza capriciosa = Pizza.getInstance();
        capriciosa.name = "Capriciosa";

        System.out.println(margharita);
        System.out.println(capriciosa);

        System.out.println(margharita.hashCode());
        System.out.println(capriciosa.hashCode());

//        Animal dog = Animal.getInstance();
//        Animal cat = Animal.getInstance();
//        cat.type = "Cat";
//        Animal duck = Animal.getInstance();
//        duck.type = "Duck";
//
//        System.out.println(dog);
//        System.out.println(cat);
//        System.out.println(duck);
    }
}

// Example
class Pizza {
    private static Pizza instance = null;

    public String name;
    private Pizza() {
        name = "Margharita";
    }

    public static Pizza getInstance() {
        if(instance == null)
            instance = new Pizza();
        return instance;
    }
    @Override
    public String toString() {
        return "Pizza name: " + name;
    }
}