package com.eugenegolobokin.sda._07_design_patterns_good_practices.p02designpatterns.singleton.example;

public class Pizza {

    private static Pizza instance = null;

    public String name;

    // singleton speciality - private constructor
    private Pizza() {
        name = "Margharita";
    }

    public static Pizza getInstance() {
        if (instance == null) {
            instance = new Pizza();
        }
        return instance;
    }

    @Override
    public String toString() {
        return "Pizza name: " + name;
    }
}