package com.eugenegolobokin.sda._07_design_patterns_good_practices.p01designprinciples.p2solid.singleResponsibility;

import java.util.ArrayList;
import java.util.List;

public class staticVsNonStatic {

        public static void main(String[] args) {
            Employee3 e1 = new Employee3(55);
            Employee3 e2 = new Employee3(66);

            System.out.println(e1.getAge());
            System.out.println(e2.getAge());

            // EmployeeStatisticsCalculator.getAverageAge(new ArrayList<>());
            // EmployeeStatisticsCalculator.getAverageSalary();
        }
    }

    class Employee3 {
        static int _age;

        public Employee3(int age) {
            _age = age;
        }

        public int getAge() {
            return _age;
        }
    }

    class EmployeeStatisticsCalculator {

        public static double getAverageAge(List<Employee> employeeList){
            return 0.0;
        }

        public static double getAverageSalary(){
            return 0.0;
        }

        public static double getAverageSalaryForSpecificAge(int age){
            return 0.0;
        }
    }
