package com.eugenegolobokin.sda._07_design_patterns_good_practices.p01designprinciples.p2solid.interfaceSegregation;

public interface Collection {
    void init();
    void get();
    void add();
    void addSeveral();
    void remove();
    void removeAll();
}


// ... what if we want to have immutable collections?
// ... it would be better to implement segregated interfaces
// ... !! need to have criterion for segregation

interface ImmutableCollection {
    void init();
    void get();
}


interface MutableCollection extends ImmutableCollection {
    void add();
    void addSeveral();
    void remove(); // remove(); does not fit for immutable collections this interface might be too broad
    void removeAll(); // removeAll(); does not fit for immutable collections this interface might be too broad
}